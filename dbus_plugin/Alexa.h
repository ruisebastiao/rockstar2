/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __Alexa_H__
#define __Alexa_H__

class AlexaProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    AlexaProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "pub.geekcentral.Rockstar.Alexa", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create Alexa remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant status READ __get_status__ )
    QDBusVariant __get_status__() { return QDBusVariant(fetchProperty("status")); }
    

    Q_PROPERTY(QDBusVariant authenticated READ __get_authenticated__ )
    QDBusVariant __get_authenticated__() { return QDBusVariant(fetchProperty("authenticated")); }
    


Q_SIGNALS:
    void error(int code,QString message);
    void userSigninPrompt(QString url);
};

class Alexa : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "pub.geekcentral.Rockstar.Alexa")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "status") {
			    Q_EMIT __statusChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "authenticated") {
			    Q_EMIT __authenticatedChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new AlexaProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

	  QObject::connect(m_ifc, SIGNAL(error(int,QString)), 
	  		this, SIGNAL(error(int,QString)));

	  QObject::connect(m_ifc, SIGNAL(userSigninPrompt(QString)), 
	  		this, SIGNAL(userSigninPrompt(QString)));

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    Alexa(QObject *parent=0) : QObject(parent), m_ifc(new AlexaProxyer("/pub/geekcentral/Rockstar/Alexa", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant status READ __get_status__  NOTIFY __statusChanged__)
    Q_PROPERTY(QVariant authenticated READ __get_authenticated__  NOTIFY __authenticatedChanged__)

    //Property read methods
    const QVariant __get_status__() { return unmarsh(m_ifc->__get_status__().variant()); }
    const QVariant __get_authenticated__() { return unmarsh(m_ifc->__get_authenticated__().variant()); }
    //Property set methods :TODO check access

public Q_SLOTS:  
    QVariant startAsking() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("startAsking"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at pub.geekcentral.Rockstar.Alexa.startAsking:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant endAsking() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("endAsking"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at pub.geekcentral.Rockstar.Alexa.endAsking:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant authenticate(const QVariant &clientId, const QVariant &clientSecret) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), clientId, "s") << marsh(QDBusArgument(), clientSecret, "s");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("authenticate"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at pub.geekcentral.Rockstar.Alexa.authenticate:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant userRedirect(const QVariant &url) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), url, "s");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("userRedirect"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at pub.geekcentral.Rockstar.Alexa.userRedirect:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }


Q_SIGNALS:
//Property changed notify signal
    void __statusChanged__(QVariant);
    void __authenticatedChanged__(QVariant);

//DBus Interface's signal
    void error(int code,QString message);
    void userSigninPrompt(QString url);
private:
    AlexaProxyer *m_ifc;
};

#endif
