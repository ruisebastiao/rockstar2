/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __Playlists_H__
#define __Playlists_H__

class PlaylistsProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    PlaylistsProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "org.mpris.MediaPlayer2.Playlists", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create Playlists remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant PlaylistCount READ __get_PlaylistCount__ )
    QDBusVariant __get_PlaylistCount__() { return QDBusVariant(fetchProperty("PlaylistCount")); }
    

    Q_PROPERTY(QDBusVariant Orderings READ __get_Orderings__ )
    QDBusVariant __get_Orderings__() { return QDBusVariant(fetchProperty("Orderings")); }
    

    Q_PROPERTY(QDBusVariant ActivePlaylist READ __get_ActivePlaylist__ )
    QDBusVariant __get_ActivePlaylist__() { return QDBusVariant(fetchProperty("ActivePlaylist")); }
    


Q_SIGNALS:
};

class Playlists : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "org.mpris.MediaPlayer2.Playlists")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "PlaylistCount") {
			    Q_EMIT __playlistCountChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Orderings") {
			    Q_EMIT __orderingsChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "ActivePlaylist") {
			    Q_EMIT __activePlaylistChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new PlaylistsProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    Playlists(QObject *parent=0) : QObject(parent), m_ifc(new PlaylistsProxyer("/org/mpris/MediaPlayer2/Playlists", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant playlistCount READ __get_PlaylistCount__  NOTIFY __playlistCountChanged__)
    Q_PROPERTY(QVariant orderings READ __get_Orderings__  NOTIFY __orderingsChanged__)
    Q_PROPERTY(QVariant activePlaylist READ __get_ActivePlaylist__  NOTIFY __activePlaylistChanged__)

    //Property read methods
    const QVariant __get_PlaylistCount__() { return unmarsh(m_ifc->__get_PlaylistCount__().variant()); }
    const QVariant __get_Orderings__() { return unmarsh(m_ifc->__get_Orderings__().variant()); }
    const QVariant __get_ActivePlaylist__() { return unmarsh(m_ifc->__get_ActivePlaylist__().variant()); }
    //Property set methods :TODO check access

public Q_SLOTS:  
    QVariant ActivatePlaylist(const QVariant &PlaylistId) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), PlaylistId, "o");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("ActivatePlaylist"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Playlists.ActivatePlaylist:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant GetPlaylists(const QVariant &Index, const QVariant &MaxCount, const QVariant &Order, const QVariant &ReverseOrder) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), Index, "u") << marsh(QDBusArgument(), MaxCount, "u") << marsh(QDBusArgument(), Order, "s") << marsh(QDBusArgument(), ReverseOrder, "b");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("GetPlaylists"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Playlists.GetPlaylists:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.mpris.MediaPlayer2.Playlists.GetPlaylists\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }


Q_SIGNALS:
//Property changed notify signal
    void __playlistCountChanged__(QVariant);
    void __orderingsChanged__(QVariant);
    void __activePlaylistChanged__(QVariant);

//DBus Interface's signal
private:
    PlaylistsProxyer *m_ifc;
};

#endif
