/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __Filesystem_H__
#define __Filesystem_H__

class FilesystemProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    FilesystemProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "org.freedesktop.UDisks2.Filesystem", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create Filesystem remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant MountPoints READ __get_MountPoints__ )
    QDBusVariant __get_MountPoints__() { return QDBusVariant(fetchProperty("MountPoints")); }
    


Q_SIGNALS:
};

class Filesystem : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "org.freedesktop.UDisks2.Filesystem")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "MountPoints") {
			    Q_EMIT __mountPointsChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new FilesystemProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    Filesystem(QObject *parent=0) : QObject(parent), m_ifc(new FilesystemProxyer("/org/freedesktop/UDisks2/Filesystem", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant mountPoints READ __get_MountPoints__  NOTIFY __mountPointsChanged__)

    //Property read methods
    const QVariant __get_MountPoints__() { return unmarsh(m_ifc->__get_MountPoints__().variant()); }
    //Property set methods :TODO check access

public Q_SLOTS:  
    QVariant SetLabel(const QVariant &label, const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), label, "s") << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("SetLabel"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Filesystem.SetLabel:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant Mount(const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Mount"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Filesystem.Mount:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.freedesktop.UDisks2.Filesystem.Mount\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }
  
    QVariant Unmount(const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Unmount"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Filesystem.Unmount:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }


Q_SIGNALS:
//Property changed notify signal
    void __mountPointsChanged__(QVariant);

//DBus Interface's signal
private:
    FilesystemProxyer *m_ifc;
};

#endif
