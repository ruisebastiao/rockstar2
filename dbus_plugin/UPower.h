/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __UPower_H__
#define __UPower_H__

class UPowerProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    UPowerProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "org.freedesktop.UPower", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create UPower remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant LidIsPresent READ __get_LidIsPresent__ )
    QDBusVariant __get_LidIsPresent__() { return QDBusVariant(fetchProperty("LidIsPresent")); }
    

    Q_PROPERTY(QDBusVariant LidIsClosed READ __get_LidIsClosed__ )
    QDBusVariant __get_LidIsClosed__() { return QDBusVariant(fetchProperty("LidIsClosed")); }
    

    Q_PROPERTY(QDBusVariant OnBattery READ __get_OnBattery__ )
    QDBusVariant __get_OnBattery__() { return QDBusVariant(fetchProperty("OnBattery")); }
    

    Q_PROPERTY(QDBusVariant DaemonVersion READ __get_DaemonVersion__ )
    QDBusVariant __get_DaemonVersion__() { return QDBusVariant(fetchProperty("DaemonVersion")); }
    


Q_SIGNALS:
    void DeviceRemoved(QDBusObjectPath arg0);
    void DeviceAdded(QDBusObjectPath arg0);
};

class UPower : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "org.freedesktop.UPower")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "LidIsPresent") {
			    Q_EMIT __lidIsPresentChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "LidIsClosed") {
			    Q_EMIT __lidIsClosedChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "OnBattery") {
			    Q_EMIT __onBatteryChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "DaemonVersion") {
			    Q_EMIT __daemonVersionChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new UPowerProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

	  QObject::connect(m_ifc, SIGNAL(DeviceRemoved(QDBusObjectPath)), 
	  		this, SIGNAL(deviceRemoved(QDBusObjectPath)));

	  QObject::connect(m_ifc, SIGNAL(DeviceAdded(QDBusObjectPath)), 
	  		this, SIGNAL(deviceAdded(QDBusObjectPath)));

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    UPower(QObject *parent=0) : QObject(parent), m_ifc(new UPowerProxyer("/org/freedesktop/UPower", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant lidIsPresent READ __get_LidIsPresent__  NOTIFY __lidIsPresentChanged__)
    Q_PROPERTY(QVariant lidIsClosed READ __get_LidIsClosed__  NOTIFY __lidIsClosedChanged__)
    Q_PROPERTY(QVariant onBattery READ __get_OnBattery__  NOTIFY __onBatteryChanged__)
    Q_PROPERTY(QVariant daemonVersion READ __get_DaemonVersion__  NOTIFY __daemonVersionChanged__)

    //Property read methods
    const QVariant __get_LidIsPresent__() { return unmarsh(m_ifc->__get_LidIsPresent__().variant()); }
    const QVariant __get_LidIsClosed__() { return unmarsh(m_ifc->__get_LidIsClosed__().variant()); }
    const QVariant __get_OnBattery__() { return unmarsh(m_ifc->__get_OnBattery__().variant()); }
    const QVariant __get_DaemonVersion__() { return unmarsh(m_ifc->__get_DaemonVersion__().variant()); }
    //Property set methods :TODO check access

public Q_SLOTS:  
    QVariant GetCriticalAction() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("GetCriticalAction"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UPower.GetCriticalAction:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.freedesktop.UPower.GetCriticalAction\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }
  
    QVariant GetDisplayDevice() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("GetDisplayDevice"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UPower.GetDisplayDevice:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.freedesktop.UPower.GetDisplayDevice\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }
  
    QVariant EnumerateDevices() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("EnumerateDevices"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UPower.EnumerateDevices:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.freedesktop.UPower.EnumerateDevices\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }


Q_SIGNALS:
//Property changed notify signal
    void __lidIsPresentChanged__(QVariant);
    void __lidIsClosedChanged__(QVariant);
    void __onBatteryChanged__(QVariant);
    void __daemonVersionChanged__(QVariant);

//DBus Interface's signal
    void deviceRemoved(QDBusObjectPath arg0);
    void deviceAdded(QDBusObjectPath arg0);
private:
    UPowerProxyer *m_ifc;
};

#endif
