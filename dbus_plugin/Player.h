/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __Player_H__
#define __Player_H__

class PlayerProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    PlayerProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "org.mpris.MediaPlayer2.Player", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create Player remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant PlaybackStatus READ __get_PlaybackStatus__ )
    QDBusVariant __get_PlaybackStatus__() { return QDBusVariant(fetchProperty("PlaybackStatus")); }
    

    Q_PROPERTY(QDBusVariant LoopStatus READ __get_LoopStatus__ WRITE __set_LoopStatus__)
    QDBusVariant __get_LoopStatus__() { return QDBusVariant(fetchProperty("LoopStatus")); }
    void __set_LoopStatus__(const QDBusVariant &v) { setProperty("LoopStatus", QVariant::fromValue(v)); }

    Q_PROPERTY(QDBusVariant Rate READ __get_Rate__ WRITE __set_Rate__)
    QDBusVariant __get_Rate__() { return QDBusVariant(fetchProperty("Rate")); }
    void __set_Rate__(const QDBusVariant &v) { setProperty("Rate", QVariant::fromValue(v)); }

    Q_PROPERTY(QDBusVariant Shuffle READ __get_Shuffle__ WRITE __set_Shuffle__)
    QDBusVariant __get_Shuffle__() { return QDBusVariant(fetchProperty("Shuffle")); }
    void __set_Shuffle__(const QDBusVariant &v) { setProperty("Shuffle", QVariant::fromValue(v)); }

    Q_PROPERTY(QDBusVariant Metadata READ __get_Metadata__ )
    QDBusVariant __get_Metadata__() { return QDBusVariant(fetchProperty("Metadata")); }
    

    Q_PROPERTY(QDBusVariant Volume READ __get_Volume__ WRITE __set_Volume__)
    QDBusVariant __get_Volume__() { return QDBusVariant(fetchProperty("Volume")); }
    void __set_Volume__(const QDBusVariant &v) { setProperty("Volume", QVariant::fromValue(v)); }

    Q_PROPERTY(QDBusVariant Position READ __get_Position__ )
    QDBusVariant __get_Position__() { return QDBusVariant(fetchProperty("Position")); }
    

    Q_PROPERTY(QDBusVariant MinimumRate READ __get_MinimumRate__ )
    QDBusVariant __get_MinimumRate__() { return QDBusVariant(fetchProperty("MinimumRate")); }
    

    Q_PROPERTY(QDBusVariant MaximumRate READ __get_MaximumRate__ )
    QDBusVariant __get_MaximumRate__() { return QDBusVariant(fetchProperty("MaximumRate")); }
    

    Q_PROPERTY(QDBusVariant CanGoNext READ __get_CanGoNext__ )
    QDBusVariant __get_CanGoNext__() { return QDBusVariant(fetchProperty("CanGoNext")); }
    

    Q_PROPERTY(QDBusVariant CanGoPrevious READ __get_CanGoPrevious__ )
    QDBusVariant __get_CanGoPrevious__() { return QDBusVariant(fetchProperty("CanGoPrevious")); }
    

    Q_PROPERTY(QDBusVariant CanPlay READ __get_CanPlay__ )
    QDBusVariant __get_CanPlay__() { return QDBusVariant(fetchProperty("CanPlay")); }
    

    Q_PROPERTY(QDBusVariant CanPause READ __get_CanPause__ )
    QDBusVariant __get_CanPause__() { return QDBusVariant(fetchProperty("CanPause")); }
    

    Q_PROPERTY(QDBusVariant CanSeek READ __get_CanSeek__ )
    QDBusVariant __get_CanSeek__() { return QDBusVariant(fetchProperty("CanSeek")); }
    

    Q_PROPERTY(QDBusVariant CanControl READ __get_CanControl__ )
    QDBusVariant __get_CanControl__() { return QDBusVariant(fetchProperty("CanControl")); }
    


Q_SIGNALS:
    void Seeked(qlonglong Position);
};

class Player : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "org.mpris.MediaPlayer2.Player")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "PlaybackStatus") {
			    Q_EMIT __playbackStatusChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "LoopStatus") {
			    Q_EMIT __loopStatusChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Rate") {
			    Q_EMIT __rateChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Shuffle") {
			    Q_EMIT __shuffleChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Metadata") {
			    Q_EMIT __metadataChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Volume") {
			    Q_EMIT __volumeChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Position") {
			    Q_EMIT __positionChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "MinimumRate") {
			    Q_EMIT __minimumRateChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "MaximumRate") {
			    Q_EMIT __maximumRateChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "CanGoNext") {
			    Q_EMIT __canGoNextChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "CanGoPrevious") {
			    Q_EMIT __canGoPreviousChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "CanPlay") {
			    Q_EMIT __canPlayChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "CanPause") {
			    Q_EMIT __canPauseChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "CanSeek") {
			    Q_EMIT __canSeekChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "CanControl") {
			    Q_EMIT __canControlChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new PlayerProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

	  QObject::connect(m_ifc, SIGNAL(Seeked(qlonglong)), 
	  		this, SIGNAL(seeked(qlonglong)));

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    Player(QObject *parent=0) : QObject(parent), m_ifc(new PlayerProxyer("/org/mpris/MediaPlayer2/Player", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant playbackStatus READ __get_PlaybackStatus__  NOTIFY __playbackStatusChanged__)
    Q_PROPERTY(QVariant loopStatus READ __get_LoopStatus__ WRITE __set_LoopStatus__ NOTIFY __loopStatusChanged__)
    Q_PROPERTY(QVariant rate READ __get_Rate__ WRITE __set_Rate__ NOTIFY __rateChanged__)
    Q_PROPERTY(QVariant shuffle READ __get_Shuffle__ WRITE __set_Shuffle__ NOTIFY __shuffleChanged__)
    Q_PROPERTY(QVariant metadata READ __get_Metadata__  NOTIFY __metadataChanged__)
    Q_PROPERTY(QVariant volume READ __get_Volume__ WRITE __set_Volume__ NOTIFY __volumeChanged__)
    Q_PROPERTY(QVariant position READ __get_Position__  NOTIFY __positionChanged__)
    Q_PROPERTY(QVariant minimumRate READ __get_MinimumRate__  NOTIFY __minimumRateChanged__)
    Q_PROPERTY(QVariant maximumRate READ __get_MaximumRate__  NOTIFY __maximumRateChanged__)
    Q_PROPERTY(QVariant canGoNext READ __get_CanGoNext__  NOTIFY __canGoNextChanged__)
    Q_PROPERTY(QVariant canGoPrevious READ __get_CanGoPrevious__  NOTIFY __canGoPreviousChanged__)
    Q_PROPERTY(QVariant canPlay READ __get_CanPlay__  NOTIFY __canPlayChanged__)
    Q_PROPERTY(QVariant canPause READ __get_CanPause__  NOTIFY __canPauseChanged__)
    Q_PROPERTY(QVariant canSeek READ __get_CanSeek__  NOTIFY __canSeekChanged__)
    Q_PROPERTY(QVariant canControl READ __get_CanControl__  NOTIFY __canControlChanged__)

    //Property read methods
    const QVariant __get_PlaybackStatus__() { return unmarsh(m_ifc->__get_PlaybackStatus__().variant()); }
    const QVariant __get_LoopStatus__() { return unmarsh(m_ifc->__get_LoopStatus__().variant()); }
    const QVariant __get_Rate__() { return unmarsh(m_ifc->__get_Rate__().variant()); }
    const QVariant __get_Shuffle__() { return unmarsh(m_ifc->__get_Shuffle__().variant()); }
    const QVariant __get_Metadata__() { return unmarsh(m_ifc->__get_Metadata__().variant()); }
    const QVariant __get_Volume__() { return unmarsh(m_ifc->__get_Volume__().variant()); }
    const QVariant __get_Position__() { return unmarsh(m_ifc->__get_Position__().variant()); }
    const QVariant __get_MinimumRate__() { return unmarsh(m_ifc->__get_MinimumRate__().variant()); }
    const QVariant __get_MaximumRate__() { return unmarsh(m_ifc->__get_MaximumRate__().variant()); }
    const QVariant __get_CanGoNext__() { return unmarsh(m_ifc->__get_CanGoNext__().variant()); }
    const QVariant __get_CanGoPrevious__() { return unmarsh(m_ifc->__get_CanGoPrevious__().variant()); }
    const QVariant __get_CanPlay__() { return unmarsh(m_ifc->__get_CanPlay__().variant()); }
    const QVariant __get_CanPause__() { return unmarsh(m_ifc->__get_CanPause__().variant()); }
    const QVariant __get_CanSeek__() { return unmarsh(m_ifc->__get_CanSeek__().variant()); }
    const QVariant __get_CanControl__() { return unmarsh(m_ifc->__get_CanControl__().variant()); }
    //Property set methods :TODO check access
    void __set_LoopStatus__(const QVariant &v) {
	    QVariant marshedValue = marsh(QDBusArgument(), v, "s");
	    m_ifc->__set_LoopStatus__(QDBusVariant(marshedValue));
	    Q_EMIT __loopStatusChanged__(marshedValue);
    }
    void __set_Rate__(const QVariant &v) {
	    QVariant marshedValue = marsh(QDBusArgument(), v, "d");
	    m_ifc->__set_Rate__(QDBusVariant(marshedValue));
	    Q_EMIT __rateChanged__(marshedValue);
    }
    void __set_Shuffle__(const QVariant &v) {
	    QVariant marshedValue = marsh(QDBusArgument(), v, "b");
	    m_ifc->__set_Shuffle__(QDBusVariant(marshedValue));
	    Q_EMIT __shuffleChanged__(marshedValue);
    }
    void __set_Volume__(const QVariant &v) {
	    QVariant marshedValue = marsh(QDBusArgument(), v, "d");
	    m_ifc->__set_Volume__(QDBusVariant(marshedValue));
	    Q_EMIT __volumeChanged__(marshedValue);
    }

public Q_SLOTS:  
    QVariant Next() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Next"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Player.Next:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant Previous() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Previous"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Player.Previous:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant Pause() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Pause"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Player.Pause:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant PlayPause() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("PlayPause"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Player.PlayPause:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant Stop() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Stop"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Player.Stop:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant Play() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Play"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Player.Play:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant Seek(const QVariant &Offset) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), Offset, "x");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Seek"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Player.Seek:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant SetPosition(const QVariant &TrackId, const QVariant &Position) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), TrackId, "o") << marsh(QDBusArgument(), Position, "x");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("SetPosition"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Player.SetPosition:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant OpenUri(const QVariant &Uri) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), Uri, "s");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("OpenUri"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Player.OpenUri:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }


Q_SIGNALS:
//Property changed notify signal
    void __playbackStatusChanged__(QVariant);
    void __loopStatusChanged__(QVariant);
    void __rateChanged__(QVariant);
    void __shuffleChanged__(QVariant);
    void __metadataChanged__(QVariant);
    void __volumeChanged__(QVariant);
    void __positionChanged__(QVariant);
    void __minimumRateChanged__(QVariant);
    void __maximumRateChanged__(QVariant);
    void __canGoNextChanged__(QVariant);
    void __canGoPreviousChanged__(QVariant);
    void __canPlayChanged__(QVariant);
    void __canPauseChanged__(QVariant);
    void __canSeekChanged__(QVariant);
    void __canControlChanged__(QVariant);

//DBus Interface's signal
    void seeked(qlonglong Position);
private:
    PlayerProxyer *m_ifc;
};

#endif
