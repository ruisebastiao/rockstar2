import QtQuick 2.6
import Material 0.3

Page {
    id: page
    title: "News"

    actions: [
        Action {
            name: "Search Location"
            iconSource: "icon://awesome/search"
        },
        Action {
            name: "Info"
            iconSource: "icon://awesome/rss"
        },
        Action {
            name: "Options"
            iconSource: "icon://awesome/cog"
        }
    ]

    actionBar {

    }

    Component.onDestruction: console.log("going away")
}
