import QtQuick 2.6
import Material 0.3

ApplicationWindow {
    visible: true
    width: 1200
    height: 800
    title: qsTr("Hello World")

    theme {
        primaryColor: "blue"
        accentColor: "red"
        tabHighlightColor: "white"
    }


    initialPage: TabbedPage {
        id: mainView
        title: "Rockstar"

        actions: [
            Action {
                name: "Account"
                iconSource: "icon://awesome/user"
                onTriggered: overlayView.open(mainView)
            }
        ]

        actionBar {

        }

        Tab {
            title: "Summary"
            Summary {
                anchors.fill: parent
                anchors.centerIn: parent
            }
        }

        Tab {
            title: "Music"
            Button {
                anchors.horizontalCenter: parent.horizontalCenter

                text: "Music"
                elevation: 1
                onClicked: pageStack.push(Qt.resolvedUrl("MusicOptions.qml"))
            }
        }

        Tab {
            title: "Weather"
            Button {
                anchors.horizontalCenter: parent.horizontalCenter

                text: "Weather"
                elevation: 1
                onClicked: pageStack.push(Qt.resolvedUrl("WeatherOptions.qml"))
            }
        }
        Tab {
            title: "News"
            Button {
                anchors.horizontalCenter: parent.horizontalCenter

                text: "News"
                elevation: 1
                onClicked: pageStack.push(Qt.resolvedUrl("NewsOptions.qml"))
            }
        }
        Tab {
            title: "Alexa"
            Button {
                anchors.horizontalCenter: parent.horizontalCenter

                text: "Alexa"
                elevation: 1
                onClicked: pageStack.push(Qt.resolvedUrl("AlexaOptions.qml"))
            }
        }

    }

    OverlayView {
        id: overlayView

        width: parent.width - dp(50)
        height: parent.height - dp(50)

        Row {
            anchors {
                top: parent.top
                right: parent.right
                rightMargin: dp(16)
            }
            height: dp(48)
            opacity: overlayView.transitionOpacity

            spacing: dp(24)

            Repeater {
                model: ["icon://awesome/home", "icon://awesome/edit", "icon://awesome/cog"]

                delegate: IconButton {
                    id: iconAction

                    iconSource: modelData

                    size: dp(24)
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }
    }
}
