import QtQuick 2.4
import Material 0.3
import Material.Extras 0.1
import Material.ListItems 0.1

Flow {
    spacing: dp(20)
    anchors.margins: dp(48)
    anchors.centerIn: parent



    WeatherCard {
    }


    SummaryCard {

        title: "hello"
        iconName: "awesome/user"

        Rectangle {
            color: "blue"
            height: dp(256)
            width: dp(256)
        }
    }

    SummaryCard {

        title: "hello"
        iconName: "awesome/user"

        Column {
            width: dp(256)
            height: childrenRect.height + dp(5)
            spacing: dp(5)

            Button {
                anchors.horizontalCenter: parent.horizontalCenter

                text: "Music"
                elevation: 1
                onClicked: pageStack.push(Qt.resolvedUrl("Music.qml"))
            }

            Button {
                anchors.horizontalCenter: parent.horizontalCenter

                text: "Weather"
                elevation: 1
                onClicked: pageStack.push(Qt.resolvedUrl("Weather.qml"))
            }

            Button {
                anchors.horizontalCenter: parent.horizontalCenter

                text: "News"
                elevation: 1
                onClicked: pageStack.push(Qt.resolvedUrl("News.qml"))
            }

            Button {
                anchors.horizontalCenter: parent.horizontalCenter

                text: "Alexa"
                elevation: 1
                onClicked: pageStack.push(Qt.resolvedUrl("Alexa.qml"))
            }

        }
    }
}

