import QtQuick 2.0
import "UrlUtilities.js" as UrlUtilities

QtObject {
    id: getHelper

    property var json: null
    property string url: ""
    property int status: 0
    property var headers: new Object

    function get(query,requestHeaders) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                getHelper.headers = request.getAllResponseHeaders();
                getHelper.status = request.status;

                var jsonResponse = JSON.parse(request.responseText);
                if (jsonResponse.errors) {
                    jsonResponse.errors.forEach(function(err) {
                        console.log("JSON error: " + err.message)
                    });
                } else {
                    getHelper.json = jsonResponse;
                }
            }
        }

        if ( query ) {
            request.open("GET", url + "?" + UrlUtilities.makeQuery(query) );
        } else {
            request.open("GET", url);
        }

        if ( requestHeaders ) {
            Object.getOwnPropertyNames(requestHeaders).forEach(function(val) {
                request.setRequestHeader(val,requestHeaders[val]);
            });
        }
        request.send();
        console.log("send")
    }
}

