#include <QCoreApplication>
#include <QDBusConnection>

#include "settings.h"
#include "weather.h"
#include "rss.h"
#include "alexa.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName(QLatin1String("Rockstartd"));
    QCoreApplication::setOrganizationDomain(QLatin1String("pub"));
    QCoreApplication::setOrganizationName(QLatin1String("geekcentral"));

    QDBusConnection::sessionBus().registerService(QLatin1String("pub.geekcentral.Rockstar"));

    Settings settings;
    Weather weather;
    Rss rss;
    Alexa alexa;

    return a.exec();
}
