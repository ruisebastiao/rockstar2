QT += core dbus network gui

CONFIG += c++11

TARGET = Rockstard
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
           settings.cpp \
           weather.cpp \
    rss.cpp \
    rssarticle.cpp \
    dbusutilities.cpp \
    alexa.cpp \
    oauth2authenticator.cpp \
    authenticaterequestjob.cpp \
    bearertokenjob.cpp


HEADERS += \
           settings.h \
           weather.h \
    rss.h \
    rssarticle.h \
    dbusutilities.h \
    alexa.h \
    oauth2authenticator.h \
    authenticaterequestjob.h \
    bearertokenjob.h


DBUS_ADAPTORS += \
                 dbus/pub.geekcentral.Rockstar.Settings.xml \
                 dbus/pub.geekcentral.Rockstar.Weather.xml \
                 dbus/pub.geekcentral.Rockstar.RssArticle.xml \
                 dbus/pub.geekcentral.Rockstar.Rss.xml \
                 dbus/pub.geekcentral.Rockstar.Alexa.xml

DISTFILES += \
             dbus/org.mpris.MediaPlayer2.xml \
             dbus/org.freedesktop.DBus.ObjectManager.xml \
             dbus/org.freedesktop.UPower.Device.xml \
             dbus/org.freedesktop.UPower.xml \
             dbus/udisks2.xml



LIBS += -L$$OUT_PWD/../3rdparty/VNTRSSReader/VNTRSSReader/ -lVNTRSSReader
INCLUDEPATH += $$PWD/../3rdparty/VNTRSSReader/VNTRSSReader
DEPENDPATH += $$PWD/../3rdparty/VNTRSSReader/VNTRSSReader

INCLUDEPATH += $$PWD/../3rdparty/multipart-parser
DEPENDPATH += $$PWD/../3rdparty/multipart-parser
