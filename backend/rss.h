#ifndef RSS_H
#define RSS_H

#include <QObject>
#include <QList>
#include "rss_adaptor.h"

class RssArticle;
class VNTRSSReader;
class VNTRSSChannel;

class Rss : public QObject
{
    Q_OBJECT
public:
    explicit Rss(QObject *parent = 0);

    Q_PROPERTY(QString feed READ feed WRITE setFeed)
    QString feed() const;
    void setFeed(const QString &feed);

    Q_PROPERTY(QList<QDBusObjectPath> articles READ articles)
    QList<QDBusObjectPath> articles() const;

private slots:
    void refreshRss();
    void loadedRSS(QList<VNTRSSChannel*> rssChannels);

private:
    void emitPropertyChanged(const QString &name, const QVariant &value);

private:
    RssAdaptor *m_adaptor;
    VNTRSSReader *m_reader;
    QString m_feed;
    QList<RssArticle*> m_articles;
    QList<QDBusObjectPath> m_articleLinks;
};

#endif // RSS_H
