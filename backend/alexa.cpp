#include "alexa.h"
#include "alexa_adaptor.h"
#include "dbusutilities.h"
#include "authenticaterequestjob.h"
#include "bearertokenjob.h"

#include "MultipartReader.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

Alexa::Alexa(QObject *parent) : QObject(parent),
    m_status(StatusOffline),
    m_authenticated(false)
{
    m_adaptor = new AlexaAdaptor(this);

    QDBusConnection::sessionBus().registerObject("/pub/geekcentral/Rockstar/Alexa", this);

    m_parser = new MultipartReader();
    m_parser->onPartBegin = Alexa::onPartBegin;
    m_parser->onPartData = Alexa::onPartData;
    m_parser->onPartEnd = Alexa::onPartEnd;
    m_parser->onEnd = Alexa::onEnd;

    m_nam = new QNetworkAccessManager(this);
    connect( m_nam, SIGNAL(finished(QNetworkReply*)),
             this, SLOT(queryFinished(QNetworkReply*)));


    //m_clientId = "amzn1.application-oa2-client.9a999ab5daf045f5a0648fc51d673496";
    //m_clientSecret = "a995a4a08f3d5da8b06748b3b471f97b2f8abcb31eae8e90c9f6511b4b664417";
    m_productId = "rockstar_qml";
}

Alexa::~Alexa()
{
    delete m_parser;
}

void Alexa::authenticate(const QString &clientId, const QString &clientSecret)
{
    m_clientId = clientId;
    m_clientSecret = clientSecret;

    AuthenticateRequestJob *authJob = new AuthenticateRequestJob(m_nam);
    connect( authJob, SIGNAL(finished()), authJob, SLOT(deleteLater()));
    connect( authJob, SIGNAL(userPrompt(QUrl)), this, SIGNAL(userSigninPrompt(QUrl)));

    setStatus(StatusAuthentication);
    authJob->authenticate(m_productId, m_clientId, m_clientSecret);
}

void Alexa::userRedirect(const QString &url)
{
    AuthenticateRequestJob *authJob = new AuthenticateRequestJob(m_nam);
    connect( authJob, SIGNAL(finished()), authJob, SLOT(deleteLater()));
    connect( authJob, &AuthenticateRequestJob::authToken, [&](const QString &token){
        m_refreshToken = token;
        setAuthenticated(true);
        setStatus(StatusIdle);

        qDebug() << Q_FUNC_INFO << m_refreshToken;


    });

    authJob->extractCode(QUrl(url), m_clientId, m_clientSecret);
}

void Alexa::queryFinished(QNetworkReply *reply)
{

}

void Alexa::onPartBegin(const MultipartHeaders &headers, void *userData) {
    Alexa *alexa = (Alexa*)userData;

    printf("onPartBegin:\n");
    MultipartHeaders::const_iterator it;
    MultipartHeaders::const_iterator end = headers.end();
    for (it = headers.begin(); it != headers.end(); it++) {
        printf("  %s = %s\n", it->first.c_str(), it->second.c_str());
    }
    printf("  aaa: %s\n", headers["aaa"].c_str());
}

void Alexa::onPartData(const char *buffer, size_t size, void *userData) {
    //printf("onPartData: (%s)\n", string(buffer, size).c_str());
    Alexa *alexa = (Alexa*)userData;

}

void Alexa::onPartEnd(void *userData) {
    Alexa *alexa = (Alexa*)userData;

    printf("onPartEnd\n");
}

void Alexa::onEnd(void *userData) {
    Alexa *alexa = (Alexa*)userData;

    printf("onEnd\n");
}

bool Alexa::authenticated() const
{
    return m_authenticated;
}

void Alexa::setAuthenticated(bool auth)
{
    if ( m_authenticated != auth ) {
        m_authenticated = auth;
        emitPropertyChanged("authenticated", m_authenticated);
    }
}

Alexa::AlexaStatus Alexa::status() const
{
    return m_status;
}

void Alexa::setStatus(Alexa::AlexaStatus status)
{
    if ( m_status != status ) {
        m_status = status;
        emitPropertyChanged("status", m_status);
    }
}

void Alexa::emitPropertyChanged(const QString &name, const QVariant &value)
{
    DBusUtilities::emitPropertyChanged(QLatin1String("pub.geekcentral.Rockstar.Alexa"),
                                       QLatin1String("/pub/geekcentral/Rockstar/Alexa"),
                                       name,
                                       value);
}

void Alexa::doAsk()
{
// get access token
    // fire off ask job
}

void Alexa::startAsking()
{

}

void Alexa::endAsking()
{

}

QString Alexa::productId() const
{
    return m_productId;
}

void Alexa::setProductId(const QString &productId)
{
    m_productId = productId;
}
