#include "oauth2authenticator.h"
#include <QStringList>
#include <QDebug>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonParseError>

OAuth2Authenticator::OAuth2Authenticator(QObject *parent) :
    QObject(parent)
{
    QUrl baseUrl(QLatin1String("https://accounts.google.com/o/oauth2/auth"));
    QUrlQuery query(baseUrl);

    query.addQueryItem(QLatin1String("client_id"), QLatin1String("589266057102.apps.googleusercontent.com"));
    query.addQueryItem(QLatin1String("scope"), QLatin1String("https://www.googleapis.com/auth/drive"));
    query.addQueryItem(QLatin1String("include_granted_scopes"), QLatin1String("true"));
    query.addQueryItem(QLatin1String("redirect_uri"), QLatin1String("https://localhost"));
    query.addQueryItem(QLatin1String("response_type"), QLatin1String("code"));

    baseUrl.setQuery(query);
    m_url = baseUrl.toString(QUrl::FullyEncoded);

    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onTokenRequestFinished(QNetworkReply*)));
}

QString OAuth2Authenticator::token() const
{
    return m_token;
}

void OAuth2Authenticator::setToken(const QString &token)
{

    QUrl baseUrl(QLatin1String("https://accounts.google.com/o/oauth2/token"));
    QNetworkRequest request;
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded; charset=utf-8");

    QUrlQuery query;
    query.addQueryItem(QLatin1String("code"),QUrl::toPercentEncoding(token));
    query.addQueryItem(QLatin1String("client_id"), QUrl::toPercentEncoding(QLatin1String("589266057102.apps.googleusercontent.com")));
    query.addQueryItem(QLatin1String("client_secret"), QUrl::toPercentEncoding(QLatin1String("bLNr0tO_4mA2-DTmdHhSRu7D")));
    query.addQueryItem(QLatin1String("redirect_uri"), QUrl::toPercentEncoding(QLatin1String("https://localhost")));
    query.addQueryItem(QLatin1String("grant_type"), QUrl::toPercentEncoding(QLatin1String("authorization_code")));

    QByteArray message = query.toString().toUtf8();

    request.setUrl(baseUrl);
    qDebug() << Q_FUNC_INFO << token;

    m_manager->post(request, message);
}

void OAuth2Authenticator::onUrlLoad(const QUrl &url)
{
    qDebug() << Q_FUNC_INFO << url;
    if (!url.isEmpty() && url.host() == QLatin1String("localhost")) {
        QUrlQuery query(url);
        setToken(query.queryItemValue(QLatin1String("code")));
    }
}

void OAuth2Authenticator::onTokenRequestFinished(QNetworkReply *reply)
{
    QByteArray message = reply->readAll();
    QJsonParseError err;
    QJsonDocument json = QJsonDocument::fromJson(message, &err);
    if ( err.error == QJsonParseError::NoError && reply->error() == QNetworkReply::NoError) {
        m_token = json.toVariant().toMap()["access_token"].toString();
        emit tokenChanged();
    } else {
        qDebug() << Q_FUNC_INFO << reply->errorString();
    }
}

QUrl OAuth2Authenticator::authUrl() const
{
    return m_url;
}


