#ifndef DBUSUTILITIES_H
#define DBUSUTILITIES_H

#include <QObject>

class DBusUtilities
{
public:
    DBusUtilities( );

    static void emitPropertyChanged(
                                    const QString &interface,
                                    const QString &path,
                                    const QString &name,
                                    const QVariant &value);
};

#endif // DBUSUTILITIES_H
