#ifndef RSSARTICLE_H
#define RSSARTICLE_H

#include <QObject>
#include <QDateTime>
#include <vntrssitem.h>

#include "rssarticle_adaptor.h"

class RssArticle : public QObject
{
    Q_OBJECT
public:
    explicit RssArticle(int id, VNTRSSItem *item, QObject *parent = 0);

    int id() const;

    Q_PROPERTY(QString title READ title)
    QString title() const;
    void setTitle(const QString &title);

    Q_PROPERTY(QString description READ description)
    QString description() const;
    void setDescription(const QString &description);

    Q_PROPERTY(QUrl image READ image)
    QUrl image() const;
    void setImage(const QUrl &image);

    Q_PROPERTY(QUrl link READ link)
    QUrl link() const;
    void setLink(const QUrl &link);

    Q_PROPERTY(QDateTime pubdate READ pubdate)
    QDateTime pubdate() const;
    void setPubdate(const QDateTime &pubdate);

    bool isEquivelent(const VNTRSSItem *article) const;
    QDBusObjectPath path() const;

private:
    void emitPropertyChanged(const QString &name, const QVariant &value);
private:
    RssArticleAdaptor *m_adaptor;
    int m_id;

    QString m_title;
    QString m_description;
    QUrl m_image;
    QUrl m_link;
    QDateTime m_pubdate;
};

#endif // RSSARTICLE_H
