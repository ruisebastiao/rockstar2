#include "weather.h"
#include "dbusutilities.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QDebug>
#include <QTimer>
#include <QJsonDocument>

Weather::Weather(QObject *parent) : QObject(parent),
    m_humidity(0),
    m_location(-1),
    m_pressure(0),
    m_temp(0),
    m_windSpeed(0),
    m_metric(false)
{
    m_adaptor = new WeatherAdaptor(this);

    QDBusConnection::sessionBus().registerObject("/pub/geekcentral/Rockstar/Weather", this);

    m_nam = new QNetworkAccessManager(this);
    connect(m_nam, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(queryFinished(QNetworkReply*)));

    QTimer *timer = new QTimer(this);
    connect( timer, SIGNAL(timeout()), this, SLOT(refreshWeather()));
    timer->setInterval(15 * 60 * 1000);
    timer->start();
}

double Weather::humidity() const
{
    return m_humidity;
}

QString Weather::iconSource() const
{
    return m_iconSource;
}

int Weather::location() const
{
    return m_location;
}

void Weather::setLocation(int loc)
{
    if ( loc != m_location ) {
        m_location = loc;
        m_zipcode = QString();
        QMetaObject::invokeMethod(this,"refreshWeather");
        emitPropertyChanged("location",m_location);
    }
}

QString Weather::name() const
{
    return m_name;
}

double Weather::pressure() const
{
    return m_pressure;
}

double Weather::temp() const
{
    return m_temp;
}

QString Weather::text() const
{
    return m_text;
}

QString Weather::windDirection() const
{
    return m_windDirection;
}

double Weather::windSpeed() const
{
    return m_windSpeed;
}

void Weather::refreshWeather()
{
    // Do not bother if we are not configured
    if ( m_location == -1 && m_zipcode == QString() ) return;

    QUrlQuery query;
    query.addQueryItem(QLatin1String("APPID"),QLatin1String("59e374bd2bb12ea21147b44d12acce9f"));
    query.addQueryItem(QLatin1String("units"), m_metric ? QLatin1String("metric") : QLatin1String("imperial"));

    if ( m_location != -1 ) {
        query.addQueryItem(QLatin1String("id"), QString::number(m_location));
    } else if ( m_zipcode != QString() ) {
        query.addQueryItem(QLatin1String("zip"), m_zipcode);
    } else {
        qWarning() << Q_FUNC_INFO << "No valid location";
    }


    QUrl url = QString(QLatin1String("http://api.openweathermap.org/data/2.5/weather"));
    url.setQuery(query);

    QNetworkRequest request;
    request.setUrl(url);

    m_nam->get(request);
    qDebug() << Q_FUNC_INFO << request.url();
}

QString getWeatherIcon( const QVariantMap &map )
{
    QString icon = map["icon"].toString();
    return QString(QLatin1String("http://openweathermap.org/img/w/%1.png")).arg(icon);
}

QString getWeatherText( const QVariantMap &map )
{
    return map["description"].toString();
}

QString getWindDirection( const QVariantMap &map )
{
    return map["deg"].toString();
}

int getWindSpeed( const QVariantMap &map )
{
    return map["speed"].toDouble();
}

void Weather::queryFinished(QNetworkReply *reply)
{
    if ( reply->error() == QNetworkReply::NoError ) {
        QJsonParseError error;
        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll(), &error );
        if ( error.error != QJsonParseError::NoError ) {
            qWarning() << Q_FUNC_INFO << error.errorString();
        } else {
            QVariantMap map = doc.toVariant().toMap();
            QVariantMap main = map["main"].toMap();
            QVariantMap weather = map["weather"].toList().first().toMap();
            QVariantMap wind = map["wind"].toMap();
            qDebug() << Q_FUNC_INFO << main;
            qDebug() << Q_FUNC_INFO << weather;
            qDebug() << Q_FUNC_INFO << wind;

            m_humidity = main["humidity"].toDouble();
            emitPropertyChanged("humidity",m_humidity);

            m_iconSource = getWeatherIcon(weather);
            emitPropertyChanged("iconSource",m_iconSource);

            m_name = map["name"].toString();
            emitPropertyChanged("name",m_name);

            m_pressure = main["pressure"].toDouble();
            emitPropertyChanged("pressure",m_pressure);

            m_temp = main["temp"].toDouble();
            emitPropertyChanged("temp",m_temp);

            m_text = getWeatherText(weather);
            emitPropertyChanged("text",m_text);

            m_windDirection = getWindDirection(wind);
            emitPropertyChanged("windDirection",m_windDirection);

            m_windSpeed = getWindSpeed(wind);
            emitPropertyChanged("windSpeed",m_windSpeed);

        }
    } else {
        qWarning() << Q_FUNC_INFO << reply->errorString();
    }
}

void Weather::emitPropertyChanged(const QString &name, const QVariant &value)
{

    DBusUtilities::emitPropertyChanged(QLatin1String("pub.geekcentral.Rockstar.Weather"),
                                       QLatin1String("/pub/geekcentral/Rockstar/Weather"),
                                       name,
                                       value);
}

QString Weather::zipcode() const
{
    return m_zipcode;
}

void Weather::setZipcode(const QString &zipcode)
{
    if ( zipcode != m_zipcode ) {
        m_zipcode = zipcode;
        m_location = -1;
        QMetaObject::invokeMethod(this,"refreshWeather");
        emitPropertyChanged("zipcode",m_zipcode);
    }

}

bool Weather::metric() const
{
    return m_metric;
}

void Weather::setMetric(bool metric)
{
    if ( m_metric != metric ) {
        m_metric = metric;
    }
}
